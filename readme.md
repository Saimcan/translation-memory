﻿# Simple Translation Memory v1.11

This project works as an API between companies' applications and translation agencies as a language translation memory.

# Prerequisites for Installation

 1. The following php version and extensions must be present on your local environment:
-   PHP >= 7.1.3
-   OpenSSL PHP Extension
-   PDO PHP Extension
-   Mbstring PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension
-   Ctype PHP Extension
-   JSON PHP Extension

2. Composer Installation:
https://getcomposer.org/

3. Laravel Installation:
* `composer global require "laravel/installer"`

4. MySQL server or MariaDB must be present.

For further information, please visit https://laravel.com/docs/5.7#installation


# Installation

1. Clone the project here:
 `git clone https://bitbucket.org/Saimcan/translation-memory`
 
2. Go to your project directory and execute
`composer update`
`composer install`

3. Edit the database connection string values on `app/config/database.php` file:

    'mysql' => [  
      'driver' => 'mysql',  
      'host' => env('DB_HOST', '127.0.0.1'),  
      'port' => env('DB_PORT', '3306'),  
      'database' => env('DB_DATABASE', 'db'),  
      'username' => env('DB_USERNAME', 'root'),  
      'password' => env('DB_PASSWORD', ''),  
      'unix_socket' => env('DB_SOCKET', ''),  
      'charset' => 'utf8mb4',  
      'collation' => 'utf8mb4_unicode_ci',  
      'prefix' => '',  
      'prefix_indexes' => true,  
      'strict' => true,  
      'engine' => null,  
    ],
    
4. For creating the database tables, go to your project folder and execute: 
`php artisan migrate`

5. Execute the following SQL Script on your local database:
https://paste.ee/p/USI03

# Api Documentation
Simple documentation is available on https://documenter.getpostman.com/view/5735837/RzZ3L2bA

# Testing With Postman
Project's methods can be tested with postman. https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop

Don't forget to make these changes in project's `app/Http/Controllers/AuthController/ApiController.php` file:

    public function respond($data, $headers = []){  
      //return response()->json($data, $this->getStatusCode(), $headers);  
      return dd(response()->json($data, $this->getStatusCode(), $headers));  
    }

Requests and Routes:

1. http://localhost/api/getwaiting/tr (**GET /waiting**)
* language is short named as tr, en, fr

2. http://localhost/api/getvalue/en/Place (**GET /value**)
* language is short named as tr, en, fr
* translation is the translation we're looking for. It's "Place" in our case.

3. http://localhost/api/postvalue (**POST /value**)
* send any JSON data as following:

    {
    	"source": "Name",
    	"source_language": "en"
    }

4. http://localhost/api/translate (**POST /translate**)
* send any JSON data as following:

    {
    	"source": "That's perfect",
    	"translation": "Bu mükemmel",
    	"source_language": "en",
    	"translation_language": "tr"
    }

