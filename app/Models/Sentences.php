<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sentences extends Model
{
    //
    protected $fillable = [
        'sentence', 'language'
    ];
}
