<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    //
    protected $fillable = [
        'source', 'translation', 'source_language', 'translation_language', 'status'
    ];
}
