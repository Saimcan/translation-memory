<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;

class ApiController extends Controller
{
    /*
     * @var int
     */
    protected $statusCode = Response::HTTP_OK;

    /*
     * @param $message
     * @return json response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function respondToBeTranslated($message){
        return $this->respond([
            'status' => 'success',
            'http_status_code' => $this->getStatusCode(),
            'status_code' => 10,
            'message' => $message
        ]);
    }

    public function respondNoTranslation($message){
        return $this->respond([
            'status' => 'success',
            'http_status_code' => $this->getStatusCode(),
            'status_code' => 20,
            'message' => $message
        ]);
    }

    public function respondAlreadyTranslated($message){
        return $this->respond([
            'status' => 'success',
            'http_status_code' => $this->getStatusCode(),
            'status_code' => 30,
            'message' => $message
        ]);
    }

    public function respondBadRequest($message){
        return $this->respond([
            'status' => 'success',
            'http_status_code' => Response::HTTP_BAD_REQUEST,
            'status_code' => 40,
            'message' => $message
        ]);
    }

    public function respondStoreSuccess($message){
        return $this->respond([
            'status' => 'success',
            'http_status_code' => Response::HTTP_CREATED,
            'status_code' => 50,
            'message' => $message
        ]);
    }

    public function respondNotFound($message = 'Not Found!'){
        return $this->respond([
            'status' => 'error',
            'http_status_code' => Response::HTTP_NOT_FOUND,
            'message' => $message,
        ]);
    }

    public function respondInternalError($message){
        return $this->respond([
            'status' => 'error',
            'http_status_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => $message,
        ]);
    }

    public function respondValidationError($message, $errors){
        return $this->respond([
            'status' => 'error',
            'http_status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => $message,
            'errors' => $errors
        ]);
    }

    public function respond($data, $headers = []){
        return response()->json($data, $this->getStatusCode(), $headers);
        //return dd(response()->json($data, $this->getStatusCode(), $headers));
    }

    public function respondWithError($message){
        return $this->respond([
            'status' => 'error',
            'http_status_code' => Response::HTTP_UNAUTHORIZED,
            'message' => $message,
        ]);
    }
}
