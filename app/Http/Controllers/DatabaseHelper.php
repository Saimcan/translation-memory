<?php

namespace App\Http\Controllers;

use \App\Models\Translation;
use App\Models\Language;

class DatabaseHelper extends ApiController
{

    public static function getWaitingToBeTranslated($language){
        return Language::join('translations', 'languages.id', '=', 'translations.source_language')
            ->select('source', 'translation', 'description_short AS source_language', 'translation_language')
            ->where('status', 2)
            ->where('description_short', '=', $language)
            ->get();
    }

    public static function getListOfTranslations($language, $content){

        return Language::join('translations', 'languages.id', '=', 'translations.source_language')
            ->select('source', 'translation')
            ->where('status', 1)
            ->where('source', 'like', $content.'%')
            ->where('description_short', '=', $language)
            ->get();
    }

    public static function isTranslationExist($term, $language){

        $result = Language::join('translations', 'languages.id', '=', 'translations.source_language')
            ->select('source', 'translation', 'description_short AS source_language', 'translation_language')
            ->where('status', 1)
            ->where('source', '=', $term)
            ->where('description_short', '=', $language)
            ->first();

        if($result !== null){
            return true;
        }else{
            return false;
        }
    }

    public static function insertTranslation($sourceLang, $transLang, $sourceText, $translationText){

        $sourceLang_id = Language::where('description_short', '=', $sourceLang)->first()->id;
        $transLang_id = Language::where('description_short', '=', $transLang)->first()->id;

        $translation = new Translation;
        $translation->source = $sourceText;
        $translation->translation = $translationText;
        $translation->source_language = $sourceLang_id;
        $translation->translation_language = $transLang_id;
        $translation->status = 1;
        $translation->save();
    }
}
