<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use Mockery\Exception;
use Symfony\Component\VarDumper\Cloner\Data;
use Validator;

class TranslationController extends ApiController
{
    //string variables
    protected $data;
    protected $lang;
    protected $result;

    public function getWaiting($lang){
        $this->result = DatabaseHelper::getWaitingToBeTranslated($lang);

        if($this->result->isEmpty()){
            $this->respondNoTranslation('No values for to be translated.');
        }else{
            //json array
            return $this->result->toJson();
        }

        return DatabaseHelper::getWaitingToBeTranslated($lang)->toJson();
    }

    public function getValue($lang, $content){
        $this->result = DatabaseHelper::getListOfTranslations($lang, $content);

        if($this->result->isEmpty()){
            //return no translations message
            $this->respondToBeTranslated('No translation available for this input.');
        }else{
            //json array
            return $this->result->toJson();
        }
    }

    public function postValue(Request $request){

        //check if data type is json
        if ($request->isJson()){
            //single json data
            $this->data = $request->input('source');
            $this->lang = $request->input('source_language');

            //check for exact translation
            $this->result = DatabaseHelper::isTranslationExist($this->data, $this->lang);

            if($this->result === true){
                //return message it exists
                $this->respondAlreadyTranslated('Translation already exists.');
            }else{
                //return message it doesn't exist
                $this->respondToBeTranslated('Translation doesn\'t exist.');
            }
        }else{
            //return not valid request message
            $this->respondBadRequest('Request type should be a valid json format.');
        }
    }

    public function postTranslation(Request $request){

        //check if data type is json
        if($request->isJson()){
            //single json data
            $this->data = $request->input('source');
            $this->lang = $request->input('source_language');

            //validate data values
            $validator = Validator::make($request->json()->all(), [
                'source' => 'required|string',
                'translation' => 'required|string',
                'source_language' => 'required|string',
                'translation_language' => 'required|string'
            ]);

            if($validator->fails()){
                $this->respondValidationError('Request type should be a valid json format.', $validator->errors()->all());
            }else{
                //check if translation exists
                $this->result = DatabaseHelper::isTranslationExist($this->data, $this->lang);
                if($this->result === true){
                    $this->respondAlreadyTranslated('Translation already exists.');
                }else{
                    try{
                        //store
                        DatabaseHelper::insertTranslation(
                            $request->input('source_language'),
                            $request->input('translation_language'),
                            $request->input('source'),
                            $request->input('translation')
                        );
                        //respond with 201
                        $this->respondStoreSuccess('Translation successfully stored.');
                    }catch (Exception $ex){
                        $this->respondInternalError('Internal server error.');
                    }
                }
            }
        }else{
            //return not valid request message
            $this->respondBadRequest('Request type should be a valid json format.');
        }
    }
}
