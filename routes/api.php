<?php

use Illuminate\Http\Request;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//GET /waiting
//Retrieves the list of values waiting to be translated.
//Will be used by translation agencies
Route::get('getwaiting/{lang}', 'TranslationController@getWaiting');

//GET /value
//Retrieves the list of translations of a value.
//Will be used by internal applications
Route::get('getvalue/{lang}/{content}', 'TranslationController@getValue');

//POST /value
//Sends a value to the translation memory to be translated.
//The response will give information if the value is already translated or needs to be translated.
//Will be used by internal applications
Route::post('postvalue', 'TranslationController@postValue');

//POST /translate
//Sends a tranlsation of a value to the translation memory.
//Will be used by translation agencies
Route::post('translate', 'TranslationController@postTranslation');