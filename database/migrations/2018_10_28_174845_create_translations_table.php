<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->longText('source');
            $table->longText('translation');

            $table->integer('source_language')->unsigned();
            $table->integer('translation_language')->unsigned()->nullable();
            $table->integer('status')->unsigned();

            $table->foreign('source_language')->references('id')->on('languages');
            $table->foreign('translation_language')->references('id')->on('languages');
            $table->foreign('status')->references('id')->on('translation_status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translations');
    }
}
